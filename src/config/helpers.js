export default {
    getParams: (url) => {
        let params = {}
        let query = url.split('?')
        if (query[1]) {
            let vars = query[1].split('&')
            vars.map((e) => {
                let pair = e.split('=')
                params[pair[0]] = decodeURIComponent(pair[1])
            })
        }
        return params
    }
}