import axios from 'axios'
import CONST from './constants'

const instance =axios.create({
    baseURL: CONST.URLS.BASE,
    timeout: 6000,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})

export default instance