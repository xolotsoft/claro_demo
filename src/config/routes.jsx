import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Detail from '../components/detail'
import Search from '../components/search'
import Home from '../components/home'
import Nav from '../components/navigation'

export default () => (
    <Nav>
        <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/detail/:id" exact component={Detail} />
            <Route path="/search" exact component={Search} />
            <Route component={Home} />
        </Switch>
    </Nav>
)