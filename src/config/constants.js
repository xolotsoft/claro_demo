export default {
    URLS: {
        BASE: 'https://mfwkweb-api.clarovideo.net',
        SERVICE: '/services/cms/level',
        DETAIL: 'services/content/data'
    },
    PARAMS: {
        BASE: {
            api_version: 'v5.86',
            authpn: 'webclient',
            authpt: 'tfg1h3j4k6fd7',
            format: 'json',
            region: 'mexico',
            device_id: 'web',
            device_category: 'web',
            device_model: 'web',
            device_type: 'web',
            device_manufacturer: 'generic',
            HKS: '9s5qq76r3g6sg4jb90l38us52',
        },
        LISTADO: {
            isCacheable: 'true',
            node: 'gen_accion',
            domain: 'https%3A%2F%2Fmfwkweb-api.clarovideo.net%2F',
            origin: 'https3A%2F%2Fwww.clarovideo.com%2F',
            user_id: '22822863'
        }
    }
}