import React from 'react'
import img from '../assets/error.png'

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props)
        this.state = { hasError: false, error: false, info: '' }
    }
    static getDerivedStateFromError(error) {
        return { hasError: true }
    }
    componentDidCatch(error, info) {
        this.setState({ error, info })
    }
    render() {
        const btn = {
            width: '120px',
            height: '40px',
            border: 'none',
            margin: '0 10px'
        }
        if (this.state.hasError) {
            return (
                <div className="container" style={{ textAlign: 'center' }}>
                    <img src={img} alt="ERROR" />
                    <h5>Sucedió un error...</h5>
                    <p>
                        Lo sentimos, estamos teniendo dificultades técnicas
                        (como puedes ver) <br />
                        prueba recargar la página, aveces funciona.
                    </p>
                    <button
                        className="btn-default"
                        style={btn}
                        onClick={() => window.history.back()}>
                        Atrás
                    </button>{' '}
                    <button
                        className="btn-default"
                        style={btn}
                        onClick={() => location.reload(true)}>
                        Recargar
                    </button>
                    <br />
                    <br />
                    <details
                        style={{ whiteSpace: 'pre-wrap', outline: 'none' }}>
                        <p>{this.state.error && this.state.error.toString()}</p>
                        <p>{this.state.info.componentStack}</p>
                    </details>
                </div>
            )
        } else {
            return this.props.children
        }
    }
}

export default ErrorBoundary
