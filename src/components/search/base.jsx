import React from 'react'
import PropTypes from 'prop-types'

import Loading from '../loading'
import api from '../../config/api'
import CONST from '../../config/constants'
import helper from '../../config/helpers'
import Item from './item'

const Base = (props) => {
    const getData = () => {
        let info = []
        for (let i = 0; i < props.genres.length; i++) {
            let params =  {...CONST.PARAMS.BASE, ...CONST.PARAMS.LISTADO}
            params['node'] = props.genres[i].id
            api
                .get(CONST.URLS.SERVICE, {
                    params
                })
                .then(response => {
                    let listado = response.data.response.modules.module
                    let content = listado.find(
                        x => x.type === 'listadoinfinito'
                    )
                    let infinite = content.components.component.find(
                        x => x.type === 'Listadoinfinito'
                    )
                    let base = CONST.URLS.BASE
                    let url = new URL(base + infinite.properties.url)
                    let parameters = helper.getParams(infinite.properties.url)
                    let params =  {...CONST.PARAMS.BASE, ...parameters}
                    api
                        .get(url.pathname, {
                            params
                        })
                        .then(response => {
                            response.data.response.groups.map((a, b) => (
                                info.push(a)
                            ))
                        }).catch(error => {
                            console.log('============= ERROR =============')
                            console.log(error)
                            console.log('=================================')
                        })
                }).catch(error => {
                    console.log('============= ERROR =============')
                    console.log(error)
                    console.log('=================================')
                })
        }
        setTimeout(() => {
            setData(info)
            filterInfo(info)
        }, 1500)
    }
    const filterInfo = (todo) => {
        if (props.search.length > 2) {
            let filtrado = todo.filter(
                x => x.title.toLowerCase().includes(props.search.toLowerCase())
            )
            setFilter(filtrado)
        } else {
            setFilter([])
        }
        props.navigationSetLoading(false)
    }
    const [data, setData] = React.useState([])
    const [filter, setFilter] = React.useState([])
    React.useEffect(() => {getData()}, [])
    React.useEffect(() => {filterInfo(data)}, [props.search])
    return (
        props.loading ? <Loading />
        : (
            <div className="search">
                <div className="row">
                    <div className="col mt-5">
                        <h3 className="search-title">
                            Resultados de tu busqueda
                        </h3>
                        <ul>
                            {filter.length > 0 ? filter.map((e, i) => (
                                <Item {...props} key={i} data={e}/>
                            )) : (
                                    <li>
                                        <h3>
                                            No hay resultados para tu busqueda
                                        </h3>
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
        )
    )
}

Base.propTypes = {
    loading: PropTypes.bool,
    navigationSetLoading: PropTypes.func,
    genres: PropTypes.array,
    search: PropTypes.string
}

export default Base