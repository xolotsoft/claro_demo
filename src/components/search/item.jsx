import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Item = (props) => {
    const [hover, setHover] = React.useState(false)
    return (
        <li
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}>
            <img src={props.data.image_large} alt=""/>
            <div
                onClick={() => {props.goToDetail(props.data)}}
                className={`hover ${hover ? 'visible' : ''}`} >
                <FontAwesomeIcon icon={['far', 'play-circle']} />
            </div>
        </li>
    )
}

export default Item