import { connect } from 'react-redux'

import Base from './base'
import {navigationSetLoading} from '../../store/navigation/actions'
import { goToDetail} from '../../store/home/actions'

const mapState = state => {
    return {
        loading: state.navigation.loading,
        genres: state.home.genres,
        search: state.navigation.search
    }
}

const mapDispatch = dispatch => {
    return {
        navigationSetLoading: value => dispatch(navigationSetLoading(value)),
        goToDetail: data => dispatch(goToDetail(data))
    }
}

export default connect(
    mapState,
    mapDispatch
)(Base)
