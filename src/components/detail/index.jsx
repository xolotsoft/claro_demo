import { connect } from 'react-redux'
import Base from './base'
import {navigationSetLoading} from '../../store/navigation/actions'

const mapState = state => {
    return {
        loading: state.navigation.loading,
        data: state.detail.data
    }
}

const mapDispatch = dispatch => {
    return {
        navigationSetLoading: value => dispatch(navigationSetLoading(value))
    }
}

export default connect(
    mapState,
    mapDispatch
)(Base)
