import React from 'react'
import PropTypes from 'prop-types'
import Loading from '../loading'
import api from '../../config/api'
import CONST from '../../config/constants'

const Base = (props) => {
    const getData = () => {
        let params =  CONST.PARAMS.BASE
        params['group_id'] = props.match.params.id
        api
            .get(CONST.URLS.DETAIL, {
                params
            })
            .then(res => {
                setData(res.data.response.group.common)
                props.navigationSetLoading(false)
            })
    }
    const [data, setData] = React.useState({})
    React.useEffect(() => {getData()}, [])
    return (
        props.loading ? <Loading />
        : (
            <div className="detail mt-5">
                <div className="row">
                    <div className="col-md-3" style={{textAlign: 'center'}}>
                        <img src={data.image_base_vertical || ''} alt=""/>
                    </div>
                    <div className="col-md-6">
                        <h3 className="detail-title">{data.title || ''}</h3>
                        <p>{data.large_description}</p>
                        <p>
                            Año:{' '}
                            {data.extendedcommon ?
                                data.extendedcommon.media.publishyear
                            : ''}
                        </p>
                        <p>Duración: {data.duration || ''}</p>
                        <button>REPRODUCIR</button>
                    </div>
                </div>
            </div>
        )
    )
}

Base.propTypes = {
    loading: PropTypes.bool,
    match: PropTypes.object,
    navigationSetLoading: PropTypes.func
}

export default Base