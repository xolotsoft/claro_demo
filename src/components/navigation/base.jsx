import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import title from '../../assets/title.svg'
import Search from './search'

const Nav = (props) => {
    return (
        <>
            <nav className="navigation noselect">
                <div className="title">
                    <Link to="/">
                        <img src={title} alt="Claro Video"/>
                    </Link>
                </div>
                <div className="tools">
                    <Search {...props}/>
                </div>
            </nav>
            <div className="container-fluid">
                {props.children}
            </div>
        </>
    )
}

Nav.propTypes = {
    children: PropTypes.any
}
export default Nav