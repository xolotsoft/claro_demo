import { connect } from 'react-redux'
import Base from './base'
import {
    navigationSetSearch
} from '../../store/navigation/actions'

const mapState = state => {
    return {
        search: state.navigation.search
    }
}

const mapDispatch = dispatch => {
    return {
        navigationSetSearch: event => dispatch(navigationSetSearch(event))
    }
}

export default connect(
    mapState,
    mapDispatch
)(Base)
