import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Search = (props) => {
    let inputRef = React.createRef()
    const [open, setOpen] = React.useState(false)
    const handleLeave = () => {
        if(props.search === '') {
            setOpen(false)
        }
    }
    return (
        <div className="search">
            <input
                id="find"
                type="text"
                ref={inputRef}
                placeholder="Buscar"
                value={props.search}
                className={`search-input ${open ? 'open' : ''}`}
                onChange={props.navigationSetSearch}
                onMouseEnter={()=> inputRef.current.focus()}
                onFocus={()=> setOpen(true)}
                onBlur={() => handleLeave()} />
            <label htmlFor="find">
                <FontAwesomeIcon icon="search" />
            </label>
        </div>
    )
}

export default Search