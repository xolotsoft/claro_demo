import { connect } from 'react-redux'
import Base from './base'
import { homeSetTest, goToDetail} from '../../store/home/actions'
import {navigationSetLoading} from '../../store/navigation/actions'

const mapState = state => {
    return {
        loading: state.navigation.loading,
        parameters: state.navigation.parameters,
        test: state.home.test,
        genres: state.home.genres,
        url: state.navigation.url,
    }
}

const mapDispatch = dispatch => {
    return {
        navigationSetLoading: value => dispatch(navigationSetLoading(value)),
        homeSetTest: value => dispatch(homeSetTest(value)),
        goToDetail: data => dispatch(goToDetail(data))
    }
}

export default connect(
    mapState,
    mapDispatch
)(Base)
