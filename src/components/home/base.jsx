import React from 'react'
import PropTypes from 'prop-types'
import Loading from '../loading'
import Slider from './slider'

const Base = (props) => {
    React.useEffect(() => {props.navigationSetLoading(false)})
    return (
        props.loading ? <Loading />
        : (
            <div className="row noselect">
                <div className="col mt-5">
                    {props.genres.map((e, i) => (
                        <Slider
                            {...props}
                            data={e}
                            key={i} />
                    ))}
                </div>
            </div>
        )
    )
}

Base.propTypes = {
    loading: PropTypes.bool,
    genres: PropTypes.array,
    navigationSetLoading: PropTypes.func
}

export default Base