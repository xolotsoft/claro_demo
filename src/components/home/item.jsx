import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Item = props => {
    const [hover, setHover] = React.useState(false)
    return (
        <li
            className="slider-item"
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            style={{left: props.left + 'px'}} >
            <div
                onClick={() => {props.goToDetail(props.data)}}
                className={`hover ${hover ? 'visible' : ''}`} >
                <FontAwesomeIcon icon={['far', 'play-circle']} />
            </div>
            <img src={props.data.image_large} alt=""/>
        </li>
    )
}

Item.propTypes = {
    data: PropTypes.any,
    left: PropTypes.number,
    goToDetail: PropTypes.func
}

export default Item