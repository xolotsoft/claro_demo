import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'

import Item from './item'
import api from '../../config/api'
import CONST from '../../config/constants'
import helper from '../../config/helpers'

class Slider extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            init: 0,
            width: 10000,
            slider: 'translate3d(0px, 0px, 0px)',
            items: []
        }
        this.handleLeft = this.handleLeft.bind(this)
        this.getList = this.getList.bind(this)
        this.handleLeft = this.handleLeft.bind(this)
        this.handleRight = this.handleRight.bind(this)
    }
    getList() {
        let params =  {...CONST.PARAMS.BASE, ...CONST.PARAMS.LISTADO}
        params['node'] = this.props.data.id
        api
            .get(CONST.URLS.SERVICE, {
                params
            })
            .then(response => {
                let listado = response.data.response.modules.module
                let content = listado.find(x => x.type === 'listadoinfinito')
                let infinite = content.components.component.find(
                    x => x.type === 'Listadoinfinito'
                )
                let base = CONST.URLS.BASE
                let url = new URL(base + infinite.properties.url)
                let parameters = helper.getParams(infinite.properties.url)
                let params =  {...CONST.PARAMS.BASE, ...parameters}
                api
                    .get(url.pathname, {
                        params
                    })
                    .then(response => {
                        this.setState({items: response.data.response.groups})
                    }).catch(error => {
                        console.log('============= ERROR =============')
                        console.log(error)
                        console.log('=================================')
                    })
            }).catch(error => {
                console.log('============= ERROR =============')
                console.log(error)
                console.log('=================================')
            })
    }
    componentDidMount() {
        let widtSlider = 40 * 300
        this.setState({width: widtSlider})
        this.getList()
    }
    handleLeft() {
        let valor =  this.state.init + 300
        this.setState({
            init:valor,
            slider:'translate3d('+ valor +'px, 0px, 0px)'
        })
        let id = '#'+ this.props.data.id +' .slider-inner li'
        let items = document.querySelectorAll(id)
        let itemsArray = Array.apply(null, items)
        itemsArray.sort((a, b) => (
            parseInt(a.style.left)> parseInt(b.style.left) ? 1 : -1
        ))
        if (true) {
            itemsArray[itemsArray.length -1].style.left =
            (parseInt(itemsArray[0].style.left) - 300) + 'px'
        }
    }
    handleRight() {
        let valor =  this.state.init - 300
        this.setState({
            init: valor,
            slider:'translate3d('+ valor +'px, 0px, 0px)'
        })
        let id = '#'+ this.props.data.id +' .slider-inner li'
        let items = document.querySelectorAll(id)
        let itemsArray = Array.apply(null, items)
        itemsArray.sort((a, b) => (
            parseInt(a.style.left)> parseInt(b.style.left) ? 1 : -1
        ))
        let condition = Math.abs(Math.abs(
            parseInt(itemsArray[0].style.left)
        ) - Math.abs(valor))
        if (condition > 300 || condition === 0) {
            itemsArray[0].style.left =(
                parseInt(itemsArray[itemsArray.length -1 ].style.left) + 300
            ) + 'px'
        }
    }
    fakeItems() {
        let items = []
        for (let index = 0; index < 20; index++) {
            items.push(
                <Item
                    {...this.props}
                    data={false}
                    key={index}
                    left={300 * index} />
            )
        }
        return items
    }
    render() {
        return(
            <div className="slider">
                <h3 className="slider-title">{this.props.data.title}</h3>
                <div className="slider-way" id={this.props.data.id}>
                    <ul
                        className="slider-inner"
                        style={{
                            transform: this.state.slider,
                            width: this.state.width
                        }}>
                        {this.state.items.length > 0 ?
                            this.state.items.map((e, i) => (
                                <Item
                                    {...this.props}
                                    data={e}
                                    key={i}
                                    left={300 * i} />
                            )) : this.fakeItems()}
                    </ul>
                </div>
                {this.state.items.length > 0 ?
                    <>
                        <div
                            className="slider-control left"
                            onClick={() => this.handleLeft()}>
                            <FontAwesomeIcon icon="chevron-left" />
                        </div>
                        <div
                            className="slider-control right"
                            onClick={() => this.handleRight()}>
                            <FontAwesomeIcon icon="chevron-right" />
                        </div>
                    </>
                : '' }
            </div>
        )
    }
}

Slider.propTypes = {
    id: PropTypes.string,
    getList: PropTypes.func,
    data: PropTypes.object
}

export default Slider