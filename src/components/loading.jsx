import React from 'react'
import load from '../assets/loading.gif'

const Loading = (props) => {
    return (
        <div className="loading">
            <img src={load} alt="Cargando"/>
        </div>
    )
}

export default Loading