import React from 'react'
import { render } from 'react-dom'
import { createBrowserHistory } from 'history'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { ConnectedRouter, routerMiddleware } from 'connected-react-router'
import { library } from '@fortawesome/fontawesome-svg-core'
import thunk from 'redux-thunk'

import logger from './middleware/logger'
import AppReducer from './store'
import ErrorBoundary from './components/error_boundary'
import Routes from './config/routes'

import './styles/style.scss'

import {
    faSearch,
    faChevronLeft,
    faChevronRight
} from '@fortawesome/free-solid-svg-icons'
import { faPlayCircle } from '@fortawesome/free-regular-svg-icons'

library.add(faSearch, faChevronLeft, faChevronRight, faPlayCircle)

const root = document.getElementById('main')
const history = createBrowserHistory()
const store = createStore(
    AppReducer(history),
    applyMiddleware(logger, thunk, routerMiddleware(history))
)

render(
    <ErrorBoundary>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Routes />
            </ConnectedRouter>
        </Provider>
    </ErrorBoundary>,
    root
)
