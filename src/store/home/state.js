export default {
    test: 'svg',
    genres: [
        {
            id: 'gen_accion',
            title: 'Acción y aventura',
            items: []
        },
        {
            id: 'gen_comedia',
            title: 'Comedia',
            items: []
        },
        {
            id: 'gen_terror',
            title: 'Terror y Suspenso',
            items: []
        },
    ]
}
