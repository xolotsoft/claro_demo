import initialState from './state'
import types from './types'

const home = (state = initialState, action) => {
    switch (action.type) {
        case types.HOME_SET_TEST:
            return {
                ...state,
                test: action.value
            }
        case types.HOME_SET_GENRES:
            return {
                ...state,
                genres: action.value
            }
        default:
            return state
    }
}
export default home
