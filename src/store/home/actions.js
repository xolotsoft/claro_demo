import { push } from 'connected-react-router'

import types from './types'
import navTypes from '../navigation/types'
import detTypes from '../detail/types'
import api from '../../config/api'
import CONST from '../../config/constants'

export const homeSetTest = value => {
    return {
        type: types.HOME_SET_TEST,
        value
    }
}

export const goToDetail = data => {
    return dispatch => {
        dispatch({
            type: navTypes.NAVIGATION_SET_LOADING,
            value: true
        })
        dispatch({
            type: detTypes.DETAIL_SET_DATA,
            value: data
        })
        dispatch(push('/detail/' + data.id))
        setTimeout(() => {
            dispatch({
                type: navTypes.NAVIGATION_SET_LOADING,
                value: false
            })
        }, 1000)
    }
}