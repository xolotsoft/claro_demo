import initialState from './state'
import types from './types'

const navigation = (state = initialState, action) => {
    switch (action.type) {
        case types.NAVIGATION_SET_SEARCH:
            return {
                ...state,
                search: action.value
            }
        case types.NAVIGATION_SET_URL:
            return {
                ...state,
                url: action.value
            }
        case types.NAVIGATION_SET_PARAMETERS:
            return {
                ...state,
                parameters: action.value
            }
        case types.NAVIGATION_SET_LOADING:
            return {
                ...state,
                loading: action.value
            }
        default:
            return state
    }
}
export default navigation
