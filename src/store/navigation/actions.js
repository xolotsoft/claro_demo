import { push } from 'connected-react-router'

import types from './types'
import api from '../../config/api'
import CONST from '../../config/constants'
import helper from '../../config/helpers'

export const navigationSetSearch = event => {
    return dispatch => {
        let value = event.target.value
        dispatch({
            type: types.NAVIGATION_SET_SEARCH,
            value
        })
        if (value.length > 2) {
            dispatch({
                type: types.NAVIGATION_SET_LOADING,
                value: true
            })
            if (window.location.pathname !== '/search') {
                dispatch(push('/search'))
            }
        }
    }
}
export const navigationSetLoading = value => {
    return {
        type: types.NAVIGATION_SET_LOADING,
        value
    }
}