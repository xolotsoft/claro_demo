import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import home from './home/reducer'
import navigation from './navigation/reducer'
import detail from './detail/reducer'

export default history =>
    combineReducers({
        router: connectRouter(history),
        home,
        navigation,
        detail
    })