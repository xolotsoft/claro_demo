import initialState from './state'
import types from './types'

const detail = (state = initialState, action) => {
    switch (action.type) {
        case types.DETAIL_SET_DATA :
            return {
                ...state,
                data: action.value
            }
        default:
            return state
    }
}
export default detail
