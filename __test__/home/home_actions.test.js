import { homeSetTest} from '../../src/store/home/actions'
import types from '../../src/store/home/types'

describe('Home Actions', () => {
    it('Home Set Test', () => {
        const test = homeSetTest('pruebas')
        expect(test).toEqual({type: types.HOME_SET_TEST, value:'pruebas'})
    })
})