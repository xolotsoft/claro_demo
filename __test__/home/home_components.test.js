import React from 'react'
import renderer from 'react-test-renderer'
import Home from '../../src/components/home/base'

describe('Home components', () => {
    it('Home Snapshot', () => {
        const render = renderer.create(<Home test="pruebas"></Home>).toJSON()
        expect(render).toMatchSnapshot()
    })
})