import types from '../../src/store/home/types'
import homeReducer from '../../src/store/home/reducer'

describe('Home Reducers', () => {
    it('HOME_SET_TEST', () => {
        let state = {test: ''}
        state = homeReducer(state, {type: types.HOME_SET_TEST, value:'pruebas'})
        expect(state).toEqual({test: 'pruebas'})
    })
})